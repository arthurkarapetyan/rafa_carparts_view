<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/10/2018
 * Time: 9:00 PM
 */

namespace AppBundle\Entity;


class CarPart
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CarPartCategory", inversedBy="carPart")
     */
    private $carPartCategory;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CarPartUniqueCode", mappedBy="carPart")
     */
    private $carPartUniqueCode;


    private $car;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CarPartCharacteristics", mappedBy="carPart", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $carPartCharacteristics;

    /**
     * @ORM\Column(type="boolean")
     */
    private $carPartIsTop;

    /**
     * @ORM\Column(type="boolean")
     */
    private $carPartIsNew;

    /**
     * @ORM\Column(type="integer")
     */
    private $carPartOrder;

    /**
     * @ORM\Column(type="string")
     * @Gedmo\Slug(fields={"carPartTitle"})
     */
    private $carPartSlug;
    /**
     * @ORM\Column(type="string")
     * @Gedmo\Translatable
     */
    private $carPartTitle;

    /**
     * @ORM\Column(type="integer")
     */
    private $carPartPrice;

    /**
     * @ORM\Column(type="string",length=500)
     * @Gedmo\Translatable
     */
    private $carPartShortText;

    /**
     * @ORM\Column(type="string")
     * @Gedmo\Translatable
     */
    private $carPartText;

    /**
     * @ORM\Column(type="string")
     */
    private $carPartOgImage;


    public function __construct()
    {
        $this->carPartCharacteristics = new ArrayCollection();
        $this->car = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    public function addCarPartCharacteristics(CarPartCharacteristics $carPartCharacteristics){
        $this->carPartCharacteristics[] = $carPartCharacteristics;
        return $this->carPartCharacteristics;
    }

    /**
     * @return mixed
     */
    public function getCarPartShortText()
    {
        return $this->carPartShortText;
    }

    /**
     * @return mixed
     */
    public function getCarPartIsNew()
    {
        return $this->carPartIsNew;
    }

    /**
     * @param mixed $carPartIsNew
     */
    public function setCarPartIsNew($carPartIsNew)
    {
        $this->carPartIsNew = $carPartIsNew;
    }

    /**
     * @return mixed
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * @param mixed $car
     */
    public function setCar($car)
    {
        $this->car = $car;
    }

    /**
     * @param mixed $carPartShortText
     */
    public function setCarPartShortText($carPartShortText)
    {
        $this->carPartShortText = $carPartShortText;
    }


    /**
     * @return mixed
     */
    public function getCarPartUniqueCode()
    {
        return $this->carPartUniqueCode;
    }

    /**
     * @return mixed
     */
    public function getCarPartPrice()
    {
        return $this->carPartPrice;
    }

    /**
     * @param mixed $carPartPrice
     */
    public function setCarPartPrice($carPartPrice)
    {
        $this->carPartPrice = $carPartPrice;
    }





    /**
     * @return mixed
     */
    public function getCarPartCategory()
    {
        return $this->carPartCategory;
    }

    /**
     * @param mixed $carPartCategory
     */
    public function setCarPartCategory($carPartCategory)
    {
        $this->carPartCategory = $carPartCategory;
    }


    /**
     * @return mixed
     */
    public function getCarPartIsTop()
    {
        return $this->carPartIsTop;
    }

    /**
     * @param mixed $carPartIsTop
     */
    public function setCarPartIsTop($carPartIsTop)
    {
        $this->carPartIsTop = $carPartIsTop;
    }

    /**
     * @return mixed
     */
    public function getCarPartOrder()
    {
        return $this->carPartOrder;
    }

    /**
     * @param mixed $carPartOrder
     */
    public function setCarPartOrder($carPartOrder)
    {
        $this->carPartOrder = $carPartOrder;
    }


    /**
     * @return mixed
     */
    public function getCarPartSlug()
    {
        return $this->carPartSlug;
    }

    /**
     * @param mixed $carPartSlug
     */
    public function setCarPartSlug($carPartSlug)
    {
        $this->carPartSlug = $carPartSlug;
    }

    /**
     * @return mixed
     */
    public function getCarPartCharacteristics()
    {
        return $this->carPartCharacteristics;
    }

    /**
     * @param mixed $carPartCharacteristics
     */
    public function setCarPartCharacteristics($carPartCharacteristics)
    {
        $this->carPartCharacteristics = $carPartCharacteristics;
    }


    /**
     * @return mixed
     */
    public function getCarPartTitle()
    {
        return $this->carPartTitle;
    }

    /**
     * @param mixed $carPartTitle
     */
    public function setCarPartTitle($carPartTitle)
    {
        $this->carPartTitle = $carPartTitle;
    }

    /**
     * @return mixed
     */
    public function getCarPartText()
    {
        return $this->carPartText;
    }

    /**
     * @param mixed $carPartText
     */
    public function setCarPartText($carPartText)
    {
        $this->carPartText = $carPartText;
    }

    /**
     * @return mixed
     */
    public function getCarPartOgImage()
    {
        return $this->carPartOgImage;
    }

    /**
     * @param mixed $carPartOgImage
     */
    public function setCarPartOgImage($carPartOgImage)
    {
        $this->carPartOgImage = $carPartOgImage;
    }

}