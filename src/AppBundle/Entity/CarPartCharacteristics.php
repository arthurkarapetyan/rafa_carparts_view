<?php
/**
 * Created by PhpStorm.
 * User: ооо
 * Date: 21.12.2017
 * Time: 18:00
 */

namespace AppBundle\Entity;



/**
 * @ORM\Entity
 * @ORM\Table(name="car_part_characteristics")
 */
class CarPartCharacteristics
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CarPart", inversedBy="carPartCharacteristics")
     */
    private $carPart;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Characteristics", inversedBy="carPartCharacteristics")
     */
    private $characteristics;

    /**
     * @ORM\Column(type="string")
     */
    private $characteristicsValue;

    public function __construct()
    {

        $this->carPart = new ArrayCollection();
        $this->characteristics = new ArrayCollection();

    }

    /**
     * @param $carPart
     * @return $this
     */
    public function addCarPart($carPart){
        $this->$carPart[] = $carPart;
        return $this;
    }

    /**
     * @param $carPart
     * @return $this
     */
    public function removeCarPart($carPart){
        $this->carPart->removeElement($carPart);
     }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getCarPart()
    {
        return $this->carPart;
    }

    /**
     * @param mixed $carPart
     */
    public function setCarPart($carPart)
    {
        $this->carPart = $carPart;
    }

    /**
     * @return mixed
     */
    public function getCharacteristics()
    {
        return $this->characteristics;
    }

    /**
     * @param mixed $characteristics
     */
    public function setCharacteristics($characteristics)
    {
        $this->characteristics = $characteristics;
    }

    /**
     * @return mixed
     */
    public function getCharacteristicsValue()
    {
        return $this->characteristicsValue;
    }

    /**
     * @param mixed $characteristicsValue
     */
    public function setCharacteristicsValue($characteristicsValue)
    {
        $this->characteristicsValue = $characteristicsValue;
    }




}