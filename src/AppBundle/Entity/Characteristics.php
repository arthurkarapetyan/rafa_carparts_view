<?php
/**
 * Created by PhpStorm.
 * User: ооо
 * Date: 21.12.2017
 * Time: 17:27
 */

namespace AppBundle\Entity;


class Characteristics
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CarPartCharacteristics", mappedBy="characteristics", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $carPartCharacteristics;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Icon",inversedBy="characteristics")
     */
    private $icon;

    /**
     * @ORM\Column(type="integer")
     */
    private $characteristicsOrder;

    /**
     * @ORM\Column(type="string")
     * @Gedmo\Translatable
     */
    private $characteristicsTitle;

    public function __construct()
    {

        $this->carPartCharacteristics = new ArrayCollection();

    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return mixed
     */
    public function getCarPartCharacteristics()
    {
        return $this->carPartCharacteristics;
    }

    /**
     * @param mixed $carPartCharacteristics
     */
    public function setCarPartCharacteristics($carPartCharacteristics)
    {
        $this->carPartCharacteristics = $carPartCharacteristics;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * @return mixed
     */
    public function getCharacteristicsOrder()
    {
        return $this->characteristicsOrder;
    }

    /**
     * @param mixed $characteristicsOrder
     */
    public function setCharacteristicsOrder($characteristicsOrder)
    {
        $this->characteristicsOrder = $characteristicsOrder;
    }

    /**
     * @return mixed
     */
    public function getCharacteristicsTitle()
    {
        return $this->characteristicsTitle;
    }

    /**
     * @param mixed $characteristicsTitle
     */
    public function setCharacteristicsTitle($characteristicsTitle)
    {
        $this->characteristicsTitle = $characteristicsTitle;
    }



}