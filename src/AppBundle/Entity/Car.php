<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/10/2018
 * Time: 8:31 PM
 */

namespace AppBundle\Entity;



use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CarPartRepository")
 * @ORM\Table(name="car")
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $carOrder;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CarManufacturer", inversedBy="car")
     */
    private $manufacturer;


    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\CarPart", mappedBy="car")
     */
    private $carPart;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CarModel", inversedBy="car")
     */
    private $carModel;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CarEngineType", inversedBy="car")
     */
    private $carEngineType;

    /**
     * @ORM\Column(type="string")
     */
     private $carTitle;

    /**
     * @ORM\Column(type="string")
     * @Gedmo\Slug(fields={"carTitle"})
     */
    private $carSlug;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCarOrder()
    {
        return $this->carOrder;
    }

    /**
     * @return mixed
     */
    public function getCarPart()
    {
        return $this->carPart;
    }

    /**
     * @param mixed $carPart
     */
    public function setCarPart($carPart)
    {
        $this->carPart = $carPart;
    }


    /**
     * @param mixed $carOrder
     */
    public function setCarOrder($carOrder)
    {
        $this->carOrder = $carOrder;
    }

    /**
     * @return mixed
     */
    public function getCarTitle()
    {
        return $this->carTitle;
    }

    /**
     * @param mixed $carTitle
     */
    public function setCarTitle($carTitle)
    {
        $this->carTitle = $carTitle;
    }

    /**
     * @return mixed
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @param mixed $manufacturer
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    /**
     * @return mixed
     */
    public function getCarModel()
    {
        return $this->carModel;
    }

    /**
     * @param mixed $carModel
     */
    public function setCarModel($carModel)
    {
        $this->carModel = $carModel;
    }

    /**
     * @return mixed
     */
    public function getCarEngineType()
    {
        return $this->carEngineType;
    }

    /**
     * @param mixed $carEngineType
     */
    public function setCarEngineType($carEngineType)
    {
        $this->carEngineType = $carEngineType;
    }

    /**
     * @return mixed
     */
    public function getCarSlug()
    {
        return $this->carSlug;
    }

    /**
     * @param mixed $carSlug
     */
    public function setCarSlug($carSlug)
    {
        $this->carSlug = $carSlug;
    }


}