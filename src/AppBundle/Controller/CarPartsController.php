<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarPartsController extends Controller
{

    /**
     * @Route("/{_locale}/parts/{slug}", name="parts_view",
     * requirements={
     *      "_locale": "es|en|de|ru",
     *  },
     *  defaults={
     *      "_locale": "es"
     *  }
     * )
     */
    public function viewAction($slug, $_locale = "es"){

        $partsRepository=$this->getDoctrine()->getRepository('AppBundle:CarPart');
        $part=$partsRepository->findOneBy(['carPartSlug'=>$slug]);
        if(empty($part)){
            return $this->redirectToRoute('parts_list');
        }
        $doctorCategoryId=$part->getCarPartCategory()->getId();
        $samePropertyQuery = $partsRepository->createQueryBuilder('p')
            ->where('p.carPartCategory = :categoryId')
            ->andWhere('p.id != :currentPropertyId')
            ->setParameter('categoryId',$doctorCategoryId)
            ->setParameter('currentPropertyId', $part->getId())
            ->orderBy('p.carPartOrder','ASC')
            ->getQuery();
        $similar= $samePropertyQuery->getResult();
        return $this->render('car-parts/view.html.twig', array(
            'part'=>$part,
            'similar'=>$similar,
            'gallery'=>json_decode($part->getCarPartGallery(),true),
        ));
    }


    /**
     * @Route("/car-part-filter/{dataType}",
     * requirements={
     *     "dataType": "1|2",
     *  },
     *  defaults={
     *     "dataType": "1",
     *  }
     * )
     */
    public function getCarPart(Request $request,$dataType=1) {

        $residenceId=$request->get('carPart');
        $carRepository=$this->getDoctrine()->getRepository('AppBundle:Car');
        $data=array();
        ////// manufacturer  ////// geting model
        if($dataType==1){
            $data=$carRepository->getCarModelResult($residenceId);
            return new Response(json_encode($data,true));
        }
        ////// model ////// geting engine type
        if($dataType==2){
            $data=$carRepository->getEngineTypeResult($residenceId);
            return new Response(json_encode($data,true));
        }
        return new Response('not');
    }

    /**
     * @Route("/{_locale}/search",
     * requirements={
     *      "_locale": "es|en|de|ru",
     *  },
     *  defaults={
     *     "_locale": "es",
     *  }
     * )
     */
    public function getSearchResultAction(Request $request,$_locale='es') {

        $filterRep=$this->getDoctrine()->getRepository("AppBundle:Car");
        $searchResult = $filterRep->getSearchResult("%".$request->get('search')."%");
        return new Response(json_encode($searchResult));
    }

}