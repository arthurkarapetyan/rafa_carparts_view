<?php

namespace AppBundle\Controller;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends Controller
{
    /**
     * @Route("/{_locale}", name="homepage",
     * requirements={
     *      "_locale": "es|en|de|ru"
     *  },
     *  defaults={
     *      "_locale": "es"
     *  }
     * )
     */
    public function indexAction(Request $request ,$_locale = "es"){

        $dateNow = new \DateTime();

        $pageRepository = $this->getDoctrine()->getRepository('AppBundle:Page');
        $slideRepository = $this->getDoctrine()->getRepository('AppBundle:Slider');
        $partsRepository = $this->getDoctrine()->getRepository('AppBundle:CarPart');
        $categoriesRepository = $this->getDoctrine()->getRepository('AppBundle:CarPartCategoryGroup');
        $page=$pageRepository->find(1);
        $topParts=$partsRepository->findBy(
            array('carPartIsTop'=>true),
            array(
                'carPartOrder' => 'ASC'
            )
            ,24);

        $newParts=$partsRepository->findBy(
            array('carPartIsNew'=>true),
            array(
                'carPartOrder' => 'ASC'
            )
            ,24);

        $slider = $slideRepository->findBy(
            array(),
            array('slideOrder' => 'ASC')
        );

        $categories = $categoriesRepository->findBy(
            array('carPartCategoryGroupIsTop'=>true),
            array('carPartCategoryGroupOrder' => 'ASC')
            ,24);

        return $this->render('page/index.html.twig', array(
            'page' => $page,
            'slider' => $slider,
            'topParts' => $topParts,
            'categories' => $categories,
            'newParts' => $newParts,
        ));

    }

    /**
     * @Route("/{_locale}/about", name="page_about",
     * requirements={
     *      "_locale": "es|en|de|ru"
     *  },
     *  defaults={
     *      "_locale": "es"
     *  }
     * )
     */
    public function aboutAction($_locale = "es"){

        $pageRepository = $this->getDoctrine()->getRepository('AppBundle:Page');
        $page = $pageRepository->findOneById(2);

        return $this->render('page/about.html.twig', array(
            'page' => $page
        ));

    }









    /**
     * @Route("/{_locale}/contact", name="page_contact",
     * requirements={
     *      "_locale": "es|en|de|ru"
     *  },
     *  defaults={
     *      "_locale": "es"
     *  }
     * )
     */
    public function contactAction(Request $request, $_locale = "es"){

        $translator = $this->get('translator');

        $pageRepository = $this->getDoctrine()->getRepository('AppBundle:Page');
        $page = $pageRepository->findOneById(3);

        $form = $this->createFormBuilder()
                ->add('name', TextType::class, array(
                    'label' => $translator->trans('Name')
                ))
                ->add('email', EmailType::class, array(
                    'label' => $translator->trans('Email')
                ))
                ->add('message', TextareaType::class, array(
                    'label' => $translator->trans('Message')
                ))
                ->add('save', SubmitType::class, array(
                    'label' => $translator->trans('Send')
                ))
                ->getForm()
            ;

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $formData = $form->getData();

            $message = new \Swift_Message('New Message From Rafa Group Services Website');
            $message
                ->setFrom('info@toolbox.am')
                ->setTo('info@toolbox.am')
                ->setBody(
                    $this->renderView(
                        'email/contact.html.twig',
                        array(
                            'name' => $formData['name'],
                            'email' => $formData['email'],
                            'message' => $formData['message'],
                        )
                    ),
                    'text/html'
                )
            ;
            $this->get('mailer')->send($message);

            $this->addFlash(
                'success',
                $translator->trans('Your message is sent')
            );
            return $this->redirectToRoute('page_contact');
        }
        return $this->render('page/contact.html.twig', array(
            'page' => $page,
            'form' => $form->createView()
        ));

    }


    /**
     * @Route("/{_locale}/cars/{slug}", name="car_list",
     * requirements={
     *      "_locale": "es|en|de|ru"
     *  },
     *  defaults={
     *      "_locale": "es"
     *  }
     * )
     */
    public function carListAction(Request $request,$_locale = "es",$slug=0,Session $session){

        $carsRepository = $this->getDoctrine()->getRepository('AppBundle:Car');
        $car = $carsRepository->findOneBy(['carSlug'=>$slug]);
        if(empty($car) && !$request->get('form')){
            return $this->redirectToRoute('homepage');
        }

        //$session = new Session();
        /**** form actions *****/
        if($request->get('form')){
            $searchFormData = $request->get('form');

            $session->set('manufacturer', $searchFormData['manufacturer']);
            $session->set('model', $searchFormData['model']);
            $session->set('type', $searchFormData['type']);
            $data=array(
                'manufacturer'=> $searchFormData['manufacturer'],
                'model'=> $searchFormData['model'],
                'type'=> $searchFormData['type'],
            );
        }

        $carPartCategoryGroup=array();
        $carPartCategory=array();
        foreach ($car->getCarPart() as $carPart){
            array_push($carPartCategoryGroup,$carPart->getCarPartCategory()->getCarPartCategoryGroup()->getCarPartCategoryGroupTitle());
            array_push($carPartCategory,$carPart->getCarPartCategory()->getCarPartCategoryTitle());
        }

        /**** end form actions *****/
        $index=0;
        $partsArr=array();
        foreach ($carPartCategoryGroup as $catGroup){
            foreach ($carPartCategory as $category){
                foreach ($car->getCarPart() as $carPart){
                    if($carPart->getCarPartCategory()->getCarPartCategoryGroup()->getCarPartCategoryGroupTitle()==$catGroup && $carPart->getCarPartCategory()->getCarPartCategoryTitle()==$category){
                        $partsArr[$catGroup][$category][$index]=$carPart;
                        $index++;
                    }
                }
            }
        }

        return $this->render('page/cars.html.twig', array(
            'car' => $car,
            'partsArr'=>$partsArr,
        ));

    }

}