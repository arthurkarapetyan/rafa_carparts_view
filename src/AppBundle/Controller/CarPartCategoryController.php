<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Blog;
use AppBundle\Entity\BlogComment;
use AppBundle\Entity\CarPartCategory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CarPartCategoryController
{



    /**
     * @Route("/{_locale}/category-group/{slug}", name="category_list",
     * requirements={
     *     "_locale": "es|en|de|ru",
     *      "paginate": "\d+"
     *  },
     *  defaults={
     *     "_locale": "es",
     *      "paginate": 1
     *  }
     * )
     */
    public function categoryListAction($slug,$_locale = "es"){

        $categoryGroupRep=$this->getDoctrine()->getRepository('AppBundle:CarPartCategoryGroup');
        $categoryGroup=$categoryGroupRep->findOneBy(array('carPartCategoryGroupSlug'=>$slug));
        if(empty($categoryGroup))
            return $this->redirectToRoute('category_group');
        return $this->render('category/category-list.html.twig', array(
            'categoryGroup' => $categoryGroup,
            'categories' => $categoryGroup->getCarPartCategory(),
        ));

    }


}