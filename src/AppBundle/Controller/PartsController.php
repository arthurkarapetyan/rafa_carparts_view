<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CarEngineType;
use AppBundle\Entity\CarManufacturer;
use AppBundle\Entity\CarModel;
use AppBundle\Entity\CarPart;
use AppBundle\Entity\FuelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Session\Session;

class PartsController extends Controller
{





    public function filterAction(Request $request,Session $session)
    {
        $em=$this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        try{
            $manufacturer=$em->getReference("AppBundle:CarManufacturer", $session->get('manufacturer'));
            $model=$em->getReference("AppBundle:CarModel", $session->get('model'));
            $type=$em->getReference("AppBundle:CarEngineType", $session->get('type'));
        }catch (\Exception $e){
        $manufacturer='';
        $model='';
        $type='';
         }
        $form = $this->createFormBuilder()
            ->add('manufacturer', EntityType::class, array(
                'class' => CarManufacturer::class,
                'choice_label' => 'carManufacturerTitle',
                'placeholder' => $translator->trans('Manufacturer'),
                'data' => $manufacturer,
                'required' => true
            ))
            ->add('model', EntityType::class, array(
                'class' => CarModel::class,
                'choice_label' => function ( ) {
                     return $entity->getCarModelTitle().' [' . $entity->getCarModelYearStart()->format('m/Y') . '-' . $entity->getCarModelYearEnd()->format('m/Y') . ']';
                },
                'placeholder' => 'Model',
                'required' => true,
                'data' => $model,
            ))
            ->add('type', EntityType::class, array(
                'class' => CarEngineType::class,
                'choice_label' => function (CarEngineType $entity) {
                    return $entity->getEngineTypeTitle().' '.$kw.' ('.$entity->getCarEngineTypeHorsePower().'PS)';
                },
                'group_by'=>function($value) {
                        return $value->getFuelType()->getFuelTypeTitle();
                },
                'placeholder' => $translator->trans('Engine Type'),
                'required' => true,
                'data' => $type,
            ))
            ->add('submit', SubmitType::class, [
                'label' => $translator->trans('Search'),
                'attr' => ['class' => 'btn btn-default pull-right'],
            ])
            ->getForm();
        return $this->render('parts/filter.html.twig', array(
            'form' => $form->createView(),
        ));

    }


}