<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2/26/2018
 * Time: 5:08 PM
 */

namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;

class CarPartRepository extends EntityRepository
{

    public function getEngineTypeResult($id){
        $em = $this->getEntityManager();
        $connection = $em->getConnection();
        $statement = $connection->prepare(
            "SELECT car.`id`,car.`engine_type_title` AS name, car.`car_engine_type_horse_power` as power, car.`car_engine_type_kw` as kw, fuel.`fuel_type_title` as fuel
                        FROM car_engine_type AS car
                        JOIN `car_engine_type_car_model` AS middle ON car.id=middle.`car_engine_type_id`
                        JOIN  `car_model` AS model ON model.`id`=middle.`car_model_id`
                        JOIN  `fuel_type` AS fuel ON fuel.`id`=car.`fuel_type_id`
                        WHERE model.`id`=:id
                        ORDER BY name  DESC
");
        $statement->bindValue('id', $id);
        $statement->execute();
            return $statement->fetchAll();
    }

    public function getSearchResult($search)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery('SELECT cp.carPartTitle       as partTitle ,
                                               cp.carPartOgImage     as image ,
                                               cp.carPartSlug        as slug ,
                                               cp.carPartPrice       as price ,
                                               cp.carPartShortText   as shortText ,
                                               cpu.carPartUniqueCode as uniqueCode 
                                                
                      FROM   AppBundle:CarPart cp
                      JOIN   AppBundle:CarPartUniqueCode cpu WITH cpu.carPart = cp.id
                      WHERE  cpu.carPartUniqueCode LIKE :search
                      ');
        $query->setParameter('search', $search);
        return $query->getResult();
    }

}