<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/4/2017
 * Time: 2:13 PM
 */

namespace AppBundle\Admin;

use AppBundle\Entity\CarModel;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;

class CarModelAdmin extends AbstractAdmin
{
    public function toString($object)
    {
        return $object instanceof CarModel
            ? $object->getcarModelTitle()
            : 'Car Model'; // shown in the breadcrumb on the create view
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('carModelOrder')
            ->add('carModelTitle')
            ->add('manufacturer', 'entity', array(
                'class' => 'AppBundle\Entity\CarManufacturer',
                'choice_label' => 'carManufacturerTitle',
            ))
            ->add('carModelYearStart', 'date', [
                'years' => range(1915, date('Y')),
            ])
            ->add('carModelYearEnd', 'date', [
                'years' => range(1915, date('Y')),
            ])
        ;

    }

}