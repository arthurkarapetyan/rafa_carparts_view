<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/4/2017
 * Time: 2:13 PM
 */

namespace AppBundle\Admin;

use AppBundle\Entity\CarPart;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use ToolBox\FileBrowserBundle\Form\Type\FileBrowserType;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface;
class CarPartAdmin extends AbstractAdmin
{
    public function toString($object)
    {
        return $object instanceof CarPart
            ? $object->getCarPartTitle()
            : 'Car Part'; // shown in the breadcrumb on the create view
    }

    public $tbOptions = array(
        'multiple' => false,
        'image_directory' => '/img/car-part',
        'thumbWidth' => 1920,
        'thumbHeight' => 1080,
        'cropOptions' => array(
            0 => array(
                'og' => array(
                    "title" => "Open Graph (facebook)",
                    "type" => "pixel",
                    "width" => 1200,
                    "height" => 630
                ),
                'thumb' => array(
                    "title" => "Thumbnail",
                    "type" => "pixel",
                    "width" => 550,
                    "height" => 550
                )
            ),
        )
    );





    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');

        if ($this->isChild()) {
            // This is the route configuration as a child
            $collection->clearExcept(['edit', 'list', 'create']);

            return;
        }
    }


    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }
        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');
        if ($this->isGranted('EDIT')) {
            $menu->addChild('Edit Car Part', [
                'uri' => $admin->generateUrl('edit', ['id' => $id])
            ]);
        }
        if ($this->isGranted('LIST')) {
            $menu->addChild('List Car Part Unique Code', [
                'uri' => $admin->generateUrl('admin.car_part_uniqgue.list', ['id' => $id])
            ]);
        }
        if ($this->isGranted('CREATE')) {
            $menu->addChild('Create Car Part Unique Code', [
                'uri' => $admin->generateUrl('admin.car_part_uniqueg.create', ['id' => $id])
            ]);
        }
    }


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('carPartCategory', 'entity', array(
                'class' => 'AppBundle\Entity\CarPartCategory',
                'choice_label' => 'carPartCategoryTitle',
            ))
            ->add('car', 'entity', array(
                'class' => 'AppBundle\Entity\Car',
                'multiple'=>true,
                'choice_label' => 'carTitle'
            ))
            ->add('carPartCharacteristics', 'sonata_type_collection',
                array(
                    'by_reference' => true,
                    'type_options' => array(
                        'delete' => true
                    )
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                )
            )
            ->add('carPartPrice')
            ->add('carPartShortText')
            ->add('carPartText', CKEditorType::class)
            ->add('carPartOgImage', FileBrowserType::class, array(
                'options' => array(
                    'multiple' => false
                ),
                'required'=>false
            ))
            ->add('carPartGallery', FileBrowserType::class, array(
                'options' => array(
                    'multiple' => true
                ),
                'required'=>false
            ))

        ;

    }


    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'carPartOrder'
    );
}