<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12/4/2017
 * Time: 2:13 PM
 */

namespace AppBundle\Admin;

use AppBundle\Entity\Car;
use AppBundle\Entity\CarModel;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class CarAdmin extends AbstractAdmin
{
    public function toString($object)
    {
        return $object instanceof Car
            ? $object->getCarTitle()
            : 'Car'; // shown in the breadcrumb on the create view
    }



    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('carOrder')
            ->add('manufacturer', 'entity', array(
                'class' => 'AppBundle\Entity\CarManufacturer',
                'choice_label' => 'carManufacturerTitle',
            ))
            ->add('carModel', 'entity', array(
                'class' => 'AppBundle\Entity\CarModel',
                'choice_label' => function (CarModel $entity) {
                    return $entity->getCarModelTitle().' [' . $entity->getCarModelYearStart()->format('m/Y') . '-' . $entity->getCarModelYearEnd()->format('m/Y') . ']';
                },
                'group_by'=>function($value) {
                    return $value->getManufacturer()->getCarManufacturerTitle();
                },

            ))
            ->add('carEngineType', 'entity', array(
                'class' => 'AppBundle\Entity\CarEngineType',
                'choice_label' => function($engine){
                    return $engine->getEngineTypeTitle().' '.$engine->getFuelType()->getFuelTypeTitle().' '.$engine->getCarEngineTypeHorsePower();
                },
                'group_by'=>function($value) {
                    return $value->getFuelType()->getFuelTypeTitle();
                },

            ))
        ;

    }

    private function CarTitle($manufacturer){
            $carTitle= $manufacturer->getManufacturer()->getCarManufacturerTitle().' '.$manufacturer->getCarModel()->getCarModelTitle().' '.$manufacturer->getCarEngineType()->getEngineTypeTitle();
            $manufacturer->setCarTitle($carTitle);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

            ->add('carTitle')
            ->add('carOrder')
            ->add('manufacturer.carManufacturerTitle',null, array('label' => 'Car Manufacturer'))
            ->add('carEngineType.EngineTypeTitle',null, array('label' => 'Car EngineType'))
            ->add('_action', null, array(
                'actions' => array(
                    'edit' => array(),
                    'delete'=>array(),
                )
            ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('carTitle')
            ->add('carOrder')
            ->add('manufacturer.carManufacturerTitle',null, array('label' => 'Car Manufacturer'))
            ->add('carEngineType.EngineTypeTitle',null, array('label' => 'Car EngineType'))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'carOrder'
    );

}